<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = ['title', 'content', 'slug', 'status', 'user_id'];
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function getTitle()
    {
        return $this->title;
    }
    
    public function store(TicketFormRequest $request)
    {
        $slug = uniqid();
        $ticket = new Ticket(array(
            'title' => $request->get('title'),
            'content' => $request->get('content'),
            'slug' => $slug
        ));

        $ticket->save();

        return redirect('/contact')->with('status', 
                'Your ticket has been created! Its unique id is: '.$slug);

    }
    
    /**
     * This will add a many to many relationship between Ticket and comments.
     * A ticket can have many comments.
     * @return type
     */
        public function comments()
    {
        return $this->morphMany('App\Comment', 'post');
    }
    
    
}
