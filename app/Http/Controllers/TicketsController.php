<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\TicketFormRequest;
use App\Ticket;

class TicketsController extends Controller
{
    /**
     * Load view to create a ticket
     * @return type
     */
    public function create()
    {
        return view('tickets.create');
    }
    
    /**
     * Add a ticket to the database
     * @param TicketFormRequest $request
     * @return type
     */
    public function store(TicketFormRequest $request)
    {
        $slug = uniqid();
        $ticket = new Ticket(array(
            'title' => $request->get('title'),
            'content' => $request->get('content'),
            'slug' => $slug
        ));

        $ticket->save();

        $data = array(
            'ticket' => $slug,
        );

        Mail::send('emails.ticket', $data, function ($message) {
            $message->from('yourEmail@domain.com', 'Learning Laravel');

            $message->to('yourEmail@domain.com')->subject('There is a new ticket!');
        });

        return redirect('/contact')->with('status',            
                'Your ticket has been created! Its unique id is: '.$slug);
    }
    
    /**
     * Show all of the tickets
     * @return type
     */
    public function index()
    {
        $tickets = Ticket::all();
        return view('tickets.index', compact('tickets'));
    }
    /**
     * Show a ticket and the assoicated comments
     * 
     * @param type $slug
     * @return type
     */
    public function show($slug)
    {
        $ticket = Ticket::whereSlug($slug)->firstOrFail();
        $comments = $ticket->comments()->get();
        return view('tickets.show', compact('ticket', 'comments'));
    }
    
    /**
     * edit a ticket
     * @param type $slug
     * @return type
     */
    public function edit($slug)
    {
        $ticket = Ticket::whereSlug($slug)->firstOrFail();
        return view('tickets.edit', compact('ticket'));
    }
    
    /**
     * Will allow us to update a ticket
     * 
     * @param type $slug
     * @param TicketFormRequest $request
     * @return type
     */
    public function update($slug, TicketFormRequest $request)
    {
        $ticket = Ticket::whereSlug($slug)->firstOrFail();
        $ticket->title = $request->get('title');
        $ticket->content = $request->get('content');
        if($request->get('status') != null) {
            $ticket->status = 0;
        } else {
            $ticket->status = 1;
        }
        $ticket->save();
        return redirect(action('TicketsController@edit', $ticket->slug))
                ->with('status', 'The ticket '.$slug.' has been updated!');  
    }
    
    public function destroy($slug)
    {
        $ticket = Ticket::whereSlug($slug)->firstOrFail();
        $ticket->delete();
        return redirect('/tickets')->with('status', 'The ticket '.$slug.' has been deleted!');
    }
}
