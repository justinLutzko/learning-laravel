<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
  //  return view('home');
//});


Route::get('/contact', 'PagesController@contact');

Route::get('/', 'PagesController@home');


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/home', 'PagesController@home');
    //Route::get('/', 'PagesController@welcome');
    Route::get('/about', 'PagesController@about');
    Route::get('/tickets', 'TicketsController@index');
    
    //Allows us to select and view one ticket.
    Route::get('/ticket/{slug?}', 'TicketsController@show');
    
    //Routes to begin updating the tickets.
    Route::get('/ticket/{slug?}/edit', 'TicketsController@edit');
    Route::post('/ticket/{slug?}/edit','TicketsController@update');
    
    Route::post('/ticket/{slug?}/delete','TicketsController@destroy');
    
    Route::get('/contact', 'TicketsController@create');
    Route::post('/contact', 'TicketsController@store');
    
    //This is the code to send an email via sound grid
    Route::get('sendemail', function () {

        $data = array(
            'name' => "Learning Laravel",
        );

        Mail::send('emails.welcome', $data, function ($message) {

            $message->from('justin.lutzko@gmail.com', 'Learning Laravel');

            $message->to('justin.lutzko@gmail.com')->subject('Learning Laravel test email');

        });

        return "Your email has been sent successfully";

    });
    
    Route::post('/comment', 'CommentsController@newComment');
    
    Route::get('users/register', 'Auth\AuthController@getRegister');
    Route::post('users/register', 'Auth\AuthController@postRegister');
    
    Route::get('users/logout', 'Auth\AuthController@getLogout');

    Route::get('users/login', 'Auth\AuthController@getLogin');
    Route::post('users/login', 'Auth\AuthController@postLogin');
    
    Route::group(array('prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'manager'), function () {
       //used for creating roles.
        Route::get('users', [ 'as' => 'admin.user.index', 'uses' => 'UsersController@index']);
        Route::get('roles', 'RolesController@index');
        Route::get('roles/create', 'RolesController@create');
        Route::post('roles/create', 'RolesController@store');
       
        //used for assigning users to roles
        Route::get('users/{id?}/edit', 'UsersController@edit');
        Route::post('users/{id?}/edit','UsersController@update');
        
        Route::get('/', 'PagesController@home');
        
        Route::get('posts', 'PostsController@index');
        Route::get('posts/create', 'PostsController@create');
        Route::post('posts/create', 'PostsController@store');
        Route::get('posts/{id?}/edit', 'PostsController@edit');
        Route::post('posts/{id?}/edit','PostsController@update');
        
        //Categories for routes
        Route::get('categories', 'CategoriesController@index');
        Route::get('categories/create', 'CategoriesController@create');
        Route::post('categories/create', 'CategoriesController@store');
    });
    
    Route::get('/blog', 'BlogController@index');
    Route::get('/blog/{slug?}', 'BlogController@show');
    
});
